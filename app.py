from flask import Flask

from .routes.auth import auth
from .routes.comments import comments
from .routes.database import check_connection
from .routes.friends import friends
from .routes.genre import genre
from .routes.language import language
from .routes.posts import posts
from .routes.songs import songs
from .routes.trends import trends

app = Flask(__name__)

# Register api routes
app.register_blueprint(auth)
app.register_blueprint(genre)
app.register_blueprint(language)
app.register_blueprint(friends)
app.register_blueprint(posts)
app.register_blueprint(comments)
app.register_blueprint(songs)
app.register_blueprint(trends)


@app.before_request
def handle_db_failure():
    check_connection()


@app.errorhandler(404)
def handle_404(e):
    return "There is nothing here", 404


@app.errorhandler(405)
def handle_405(e):
    return "Wrong request method", 405
