from sys import path

path.insert(0, 'routes')
from routes.database import access_database
from random import sample

db = access_database()
# Buffered when set to true allows multiple queries without collision
cursor = db.cursor(buffered=True)


def calc_mood(songs, mood):
    recommendation = {}
    for i in mood:
        recommendation[i] = set()
    for i in songs:
        if i[1] < 0.2:
            recommendation[mood[6]].add(i[0])
        elif i[1] < 0.4:
            recommendation[mood[7]].add(i[0])
            recommendation[mood[5]].add(i[0])
        elif i[1] < 0.6:
            recommendation[mood[0]].add(i[0])
        elif i[1] < 0.8:
            recommendation[mood[1]].add(i[0])
            recommendation[mood[3]].add(i[0])
        else:
            recommendation[mood[2]].add(i[0])
        if i[2] < 2:
            recommendation[mood[4]].add(i[0])
        elif i[2] < 4:
            recommendation[mood[3]].add(i[0])
            recommendation[mood[5]].add(i[0])
        elif i[2] < 7:
            recommendation[mood[2]].add(i[0])
            recommendation[mood[6]].add(i[0])
        elif i[2] < 8:
            recommendation[mood[1]].add(i[0])
            recommendation[mood[7]].add(i[0])
        else:
            recommendation[mood[0]].add(i[0])
        if i[3] < 0.2:
            recommendation[mood[4]].add(i[0])
        elif i[3] < 0.3:
            recommendation[mood[5]].add(i[0])
        elif i[3] < 0.4:
            recommendation[mood[3]].add(i[0])
        elif i[3] < 0.5:
            recommendation[mood[6]].add(i[0])
        elif i[3] < 0.6:
            recommendation[mood[7]].add(i[0])
        elif i[3] < 0.7:
            recommendation[mood[2]].add(i[0])
        elif i[3] < 0.8:
            recommendation[mood[1]].add(i[0])
        else:
            recommendation[mood[0]].add(i[0])
        if i[4] < 80:
            recommendation[mood[6]].add(i[0])
        elif i[4] < 110:
            recommendation[mood[4]].add(i[0])
            recommendation[mood[5]].add(i[0])
            recommendation[mood[7]].add(i[0])
        elif i[4] < 140:
            continue
        elif i[4] < 170:
            recommendation[mood[1]].add(i[0])
            recommendation[mood[2]].add(i[0])
        else:
            recommendation[mood[0]].add(i[0])
            recommendation[mood[3]].add(i[0])
    return recommendation


def for_chart(recommendation):
    total = int(1)
    result = [0] * 8
    for i, j in recommendation.items():
        total += len(j)
    k = int(0)
    maxvalue = int(0)
    maxkey = ""
    for i, j in recommendation.items():
        result[k] += len(j) / total * 100
        if maxvalue < result[k]:
            maxkey = i
        k += 1
    return result


def for_collabreco(recommendation, result):
    reco = []
    maxvalue = 0
    for i in result:
        if maxvalue < i:
            maxvalue = i
    k = int(0)
    for i, j in recommendation.items():
        if result[k] == maxvalue:
            reco.extend(j)
            break
        k += 1
    return reco


def select_songs(days, username):
    songs = []
    cursor.execute(
        "select songs.song_id,songs.energy,songs.s_key,songs.valance,songs.tempo,date(timestamp) FROM activity "
        "INNER JOIN songs on activity.song_id=songs.song_id where username=%s and status=0 "
        "and datediff(CURRENT_DATE(),date(timestamp)) <= %s",
        (username, days))
    for song in cursor:
        songs.append(song)
    return songs


def recommend(username):
    songs = []
    try:
        cursor.execute(
            "select songs.song_id,songs.energy,songs.s_key,songs.valance,songs.tempo,date(timestamp) FROM activity "
            "INNER JOIN songs on activity.song_id=songs.song_id where username=%s",
            (username,))
        for song in cursor:
            songs.append(song)
        result = for_chart(calc_mood(songs, ["Happy", "Exuberent", "Energetic", "Frantic", "Sad", "Depression", "Calm",
                                             "Contentment"]))
        songs = []
        cursor.execute(
            "select songs.song_id,songs.energy,songs.s_key,songs.valance,songs.tempo,date(timestamp) FROM activity "
            "INNER JOIN songs on activity.song_id=songs.song_id where username<>%s",
            (username,))
        for song in cursor:
            songs.append(song)
        reco = for_collabreco(calc_mood(songs,
                                        ["Happy", "Exuberent", "Energetic", "Frantic", "Sad", "Depression", "Calm",
                                         "Contentment"]), result)
        return sample(reco, 10)
    except KeyError:
        return [0] * 8


def chart(username, days):
    try:
        ans = [list(), list(), list(), list()]
        for i in range(4):
            ans[i].extend(for_chart(calc_mood(select_songs(days[i], username),
                                              ["Happy", "Exuberent", "Energetic", "Frantic", "Sad", "Depression",
                                               "Calm", "Contentment"])))
        return ans
    except KeyError:
        return [0] * 8
