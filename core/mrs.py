from sklearn.ensemble.weight_boosting import AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC

from .analysis import MusicBrain

classifiers = [
    SVC(),
    AdaBoostClassifier(),
    MLPClassifier(),
    GaussianNB(),
]


def find_best_model(df):
    X = df.iloc[:, 2:-1]
    y = df.iloc[:, -1]
    index = 0
    best_model = None
    max = 0
    for model in classifiers:
        model.fit(X, y)
        score = model.score(X, y)
        index += 1
        if max < score:
            best_model = model
            max = score
    print(best_model, max)
    return best_model


def generate_recommendations(df, token, seed, count=50):
    mb = MusicBrain(token)
    model = find_best_model(df)
    print("Seed track:", seed)
    recommendations = set()
    for i in mb.fetch_playlists(count, seed, df.iloc[:, 2:-1]):
        prediction = model.predict([i[2:]])
        if prediction:
            recommendations.add(i[0])
    playlist_id = mb.create_playlist()
    return mb.add_songs_to_playlist(playlist_id, recommendations), playlist_id
