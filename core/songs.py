# from .mrs import get_recommendations
from sys import path

from core.mrs import generate_recommendations

path.insert(0, 'core')
path.insert(0, 'routes')
from routes.database import access_database
import pandas as pd

db = access_database()
# Buffered when set to true allows multiple queries without collision
cursor = db.cursor(buffered=True)


def get_all_songs(username):
    query = '''
    select a.song_id,
       s.name,
       s.danceability,
       s.energy,
       s.s_key,
       s.loudness,
       s.mode,
       s.speechiness,
       s.instrumentalness,
       s.valance,
       s.acousticness,
       s.tempo,
       max(a.status) as status
    from activity a
             join songs s on a.song_id = s.song_id and a.username = %s
    group by a.song_id, s.name, s.danceability, s.energy, s.s_key, s.loudness, s.mode, s.speechiness, s.instrumentalness,
             s.valance, s.acousticness, s.tempo
    '''
    cursor.execute(query, (username,))
    for i in cursor:
        yield i


def recommendation_engine(username, token):
    songs_list = []
    for i in get_all_songs(username):
        songs_list.append(i)
    df = pd.DataFrame(songs_list,
                      columns=["song_id", "name", "danceability", "energy", "s_key", "loudness",
                               "mode", "speechiness", "instrumentalness", "valance", "acousticness",
                               "tempo", "status"])
    random_seed_song = ",".join(map(str, df.sample(2)['song_id']))
    status, playlist_id = generate_recommendations(df, token=token, seed=random_seed_song)
    return status, playlist_id
