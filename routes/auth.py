import json
import os

import bcrypt
import mysql.connector as sql
import requests
from flask import Blueprint, request

from .database import access_database

auth = Blueprint('auth', __name__, url_prefix="/auth")
db = access_database()
# Buffered when set to true allows multiple queries without collision
cursor = db.cursor(buffered=True)


@auth.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    try:
        username = data['username']
        password = data['password'].encode()
        cursor.execute("select * from users where username=%s", (username,))
        cursor.execute("select * from users where username=%s", (username,))
        print(cursor.rowcount)
        if cursor.rowcount < 1:
            return ({"status": 0, "message": "No account found"}), 200
        else:
            for data in cursor:
                if bcrypt.checkpw(password, data[1].encode()):
                    return ({"status": 1, "username": data[0], "token": data[4], "playlist_id": data[5], "collab_reco": data[6]}), 200
                else:
                    return ({"status": 0, "message": "Incorrect password"}), 200

    except KeyError:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@auth.route('/signup', methods=['POST'])
def signup():
    data = request.get_json()
    try:
        username = data['username']
        password = data['password'].encode()
        hashed_password = bcrypt.hashpw(password, bcrypt.gensalt())
        email = data['email']
        try:
            cursor.execute("insert into users(username,password,email) values(%s,%s,%s)",
                           (username, hashed_password, email))
            db.commit()
            return ({"status": 1, "message": "Account created"}), 200
        except sql.errors.IntegrityError:
            return ({"status": 0, "message": "Account creation failed, username already taken"}), 200
    except KeyError:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@auth.route('/year/add', methods=['POST'])
def year():
    data = request.get_json()
    try:
        username = data['username']
        yr = data['dob']
        cursor.execute("update users set dob=%s where username=%s", (yr, username))
        if cursor.rowcount == 1:
            db.commit()
            return ({"status": 1, "message": "Year entered"}), 200
        else:
            return ({"status": 0, "message": "User does not exist"}), 200
    except KeyError:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@auth.route('/token', methods=['GET'])
def get_user_token():
    # Fetch user token to access user's spotify data
    code = request.args.get('code')
    username = request.args.get('username')
    client_id = os.environ.get('client.id')
    client_secret = os.environ.get('client.secret')
    redirect_uri = os.environ.get('redirect.uri')
    scope = os.environ.get('scope')

    if code is not None and username is not None:
        url = "https://accounts.spotify.com/api/token"
        payload = "grant_type=authorization_code&code=" + code + \
                  "&redirect_uri=" + redirect_uri + "&client_id=" + \
                  client_id + "&client_secret=" + client_secret + "&scope=" + scope
        headers = {
            'content-type': "application/x-www-form-urlencoded",
        }
        response = requests.request("POST", url, data=payload, headers=headers)
        refresh_token = json.loads(response.content).get('refresh_token')
        if refresh_token is not None:
            cursor.execute(
                "update users set token=%s where username=%s", (refresh_token, username))
            db.commit()
            return ({"status": 1, "refresh_token": refresh_token}), 200
        else:
            return ({"status": 0, "message": "Error while fetching token"}), 400

    else:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400
