import mysql.connector as sql
from flask import Blueprint, request

from .database import access_database

db = access_database()

friends = Blueprint('friends', __name__, url_prefix="/friends")

# Buffered when set to true allows multiple queries without collision
cursor = db.cursor(buffered=True)


# follow user
@friends.route('/new', methods=['POST'])
def new():
    data = request.get_json()
    try:
        username = data['username']
        friend = data['friend']
        try:
            cursor.execute(
                "insert into friends(username,friend) values(%s,%s)", (username, friend))
            db.commit()
            return ({"status": 1, "message": "Friend Request sent"}), 200
        except sql.errors.IntegrityError:
            return ({"status": 0, "message": "Friend Request already sent"}), 200
    except KeyError:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


# accept follow request
@friends.route('/accept', methods=['POST'])
def accepted():
    data = request.get_json()
    try:
        username = data['username']
        friend = data['friend']
        cursor.execute(
            "update friends set status=%s where username=%s and friend=%s and status=%s", (1, friend, username, 0))
        print(cursor.rowcount)
        if cursor.rowcount == 1:
            db.commit()
            return ({"status": 1, "message": "You and " + friend + " are now friends"}), 200
        else:
            return ({"status": 0, "message": "Something went wrong"}), 200
    except KeyError:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


# unfollow a user
@friends.route('/unfriend', methods=['DELETE'])
def unfriend():
    data = request.get_json()
    try:
        username = data['username']
        friend = data['friend']
        cursor.execute(
            "delete from friends where username=%s and friend=%s and status=%s", (username, friend, 1))
        if cursor.rowcount == 1:
            db.commit()
            return ({"status": 1, "message": "Unfriended " + friend}), 200
        else:
            return ({"status": 0, "message": "Something went wrong"}), 200
    except KeyError:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


# list users who the given username is following
@friends.route('/following', methods=['GET'])
def list_following():
    username = request.args.get('username')
    if username is not None:
        friend_list = []
        cursor.execute(
            "select friend from friends where username=%s and status=%s", (username, 1))
        for i in cursor:
            friend_list.append(i[0])
        return ({"status": 1, "friends": friend_list}), 200
    else:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


# list users who follow the given username
@friends.route('/followers', methods=['GET'])
def list_followers():
    username = request.args.get('username')
    if username is not None:
        friend_list = []
        cursor.execute(
            "select username from friends where friend=%s and status=%s", (username, 1))
        for i in cursor:
            friend_list.append(i[0])
        return ({"status": 1, "friends": friend_list}), 200
    else:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@friends.route('/count')
def friends_count():
    username = request.args.get('username')
    if username is not None:
        cursor.execute("SELECT count(*) FROM friends WHERE username=%s and status=1", (username,))
        following_count = 0
        for i in cursor:
            following_count = i[0]
        cursor.execute("SELECT count(*) FROM friends WHERE friend=%s and status=1", (username,))
        followers_count = 0
        for i in cursor:
            followers_count = i[0]
        return ({"status": 1, "followers": followers_count, "following": following_count}), 200
    else:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


# list users who have given the current user a follow requested
@friends.route('/requested', methods=['GET'])
def list_requested():
    username = request.args.get('username')
    if username is not None:
        friend_list = []
        cursor.execute(
            "select username from friends where friend=%s and status=0", (username,))
        for i in cursor:
            friend_list.append(i[0])
        return ({"status": 1, "friends": friend_list}), 200
    else:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400
