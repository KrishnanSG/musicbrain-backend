import mysql.connector as sql
from flask import Blueprint, request

from .database import access_database

db = access_database()

genre = Blueprint('genre', __name__, url_prefix="/genre")

# Buffered when set to true allows multiple queries without collision
cursor = db.cursor(buffered=True)


@genre.route('/add', methods=['POST'])
def add_genre():
    data = request.get_json()
    try:
        genre = data['genre']
        username = data['username']
        if isinstance(genre, list):
            for item in genre:
                cursor.execute(
                    "insert into genres(username,genre) values(%s,%s)", (username, item))
            db.commit()
            return ({"status": 1, "message": "genre added"}), 200
        else:
            return ({"status": 0, "message": "Invalid format,got string expected list"}), 400
    except sql.errors.IntegrityError:
        return ({"status": 0, "message": "Duplicate entry"}), 200
    except KeyError:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@genre.route('/delete', methods=['DELETE'])
def delete_genre():
    data = request.get_json()
    try:
        username = data['username']
        genre = data['genre']
        if isinstance(genre, list):
            for item in genre:
                cursor.execute(
                    "delete from genres where username=%s and genre=%s;", (username, item))
            db.commit()
            if cursor.rowcount >= 1:
                return ({"status": 1, "message": "Genre preference deleted"}), 200
            else:
                return ({"status": 0, "message": "Username or genre doesn't exist"}), 400
        else:
            return ({"status": 0, "message": "Invalid format,got string expected list"}), 400
    except KeyError:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@genre.route('/view', methods=['GET'])
def view_genres():
    username = request.args.get('username')
    if username is not None:
        genres = []
        cursor.execute(
            "select genre from genres where username=%s;", (username,))
        for genre in cursor:
            genres.append(genre[0])
        return ({"status": 1, "genres": genres}), 200
    else:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400
